'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
exports.LanguageConnection = void 0;
const net = require("net");
const winreg = require("winreg");
const vscode = require("vscode");
const node_1 = require("vscode-languageclient/node");
const vscode_1 = require("vscode");
const extension_1 = require("./extension");
const completion = require("./completion");
const events_1 = require("events");
const path = require("path");
let omlTerminal;
class LanguageConnection {
    constructor() {
        this.socket = '';
        this.omlPath = '';
        this.exeName = '';
        this.omlRoot = '';
        this.omlThirdParty = '';
        this.platForm = '';
        this.emitter = new events_1.EventEmitter();
        this.cbmap = new Map;
        this.isRunning = false;
        this.client = {};
        this.outputChannel = {};
        this.setupEnv();
        this.startConnection();
        this.compData = new completion.CompletionData('', '');
        vscode.languages.registerCompletionItemProvider(['oml'], new completion.CompletionItemProvider(this.compData), '');
        const signTriggerCharacters = ['(', ','];
        vscode.languages.registerSignatureHelpProvider(['oml'], new completion.SignatuareHelpProvider(), ...signTriggerCharacters);
        extension_1.extensionContext.subscriptions.push(vscode.window.onDidCloseTerminal(this.deleteTerminal, this));
    }
    dispose() {
        return this.closeConnection();
    }
    setupEnv() {
        const config = vscode.workspace.getConfiguration('OML');
        let exePath = config.get('OML_EXE');
        if (exePath !== undefined) {
            this.omlPath = exePath;
        }
        else {
            this.omlPath = String(process.env['OML_EXE']);
        }
        this.omlRoot = path.join(this.omlPath, '../../../../');
        this.platForm = "win64";
        if (process.platform !== 'win32') {
            this.platForm = "linux64";
        }
        let rootDir = this.omlRoot;
        this.exeName = path.basename(this.omlPath);
        if (this.exeName === "Compose.exe") {
            process.env['HW_ROOTDIR'] = rootDir;
            process.env['ALTAIR_HOME'] = rootDir;
            process.env['HW_UNITY_ROOTDIR'] = path.join(rootDir, 'hwx');
            process.env['OML_APPDIR'] = path.join(rootDir, 'hwx');
            process.env['OML_HELP'] = path.join(rootDir, 'hwx/help/compose/help/en_us/topics/reference/oml_language/');
            process.env['HW_FRAMEWORK'] = path.join(rootDir, '../common/framework/', this.platForm, 'hwx');
            process.env['QT_QPA_PLATFORM_PLUGIN_PATH'] = path.join(String(process.env['HW_FRAMEWORK']), 'bin', this.platForm, 'platforms');
        }
        else if (this.exeName === "omlconsole.exe") {
            process.env['OML_HELP'] = path.join(rootDir, 'help/win/en/topics/reference/oml_language/');
            process.env['OML_THIRDPARTY'] = path.join(rootDir, 'third_party');
            process.env['OML_INTEL_COMP'] = config.get('OML_INTEL_COMP');
            process.env['OML_INTEL_MKL'] = config.get('OML_INTEL_MKL');
            process.env['OML_FFTW'] = config.get('OML_FFTW');
            process.env['OML_MATIO'] = config.get('OML_MATIO');
            process.env['OML_HDF'] = config.get('OML_HDF');
            process.env['OML_QHULL'] = config.get('OML_QHULL');
            this.omlThirdParty = String(process.env['OML_THIRDPARTY']);
        }
        else {
            vscode_1.window.showErrorMessage('OML executable not found, pls check OML_EXE setting.');
        }
    }
    isConnectionAlive() {
        return this.isRunning;
    }
    addListener(name, callback) {
        if (this.cbmap.has(name) === true) {
            this.emitter.removeListener(name, this.cbmap.get(name));
        }
        this.cbmap.set(name, callback);
        this.emitter.addListener(name, callback);
    }
    setClient(client) {
        this.client = client;
    }
    async recvData(data) {
        let str = (data.toString()).replace(/}{/g, '},{'); // segregate multiple messages
        str = str.replace(/\n/g, '\\n'); // hack for new lines
        let jObjs = JSON.parse('[' + str + ']');
        //let jObjs = JSON.parse('[' + (data.toString()).replace(/}{/g, '},{') + ']');
        for (var i = 0; i < jObjs.length; i++) {
            if ('' === jObjs[i]) {
                continue;
            }
            switch (jObjs[i].type) {
                case 'status': {
                    this.isRunning = jObjs[i].data === 'ready';
                    break;
                }
                case 'keywordslist': {
                    this.compData.setKeywords(jObjs[i].data);
                    break;
                }
                case 'functionslist': {
                    this.compData.setFunctions(jObjs[i].data);
                    break;
                }
                case 'signatures': {
                    const signs = jObjs[i].data;
                    this.emitter.emit('signature', signs);
                }
            }
        }
    }
    async sendRequest(request) {
        if (!this.isConnectionAlive()) {
            await this.restartConnection();
        }
        this.socket.write(request);
    }
    async getOmlRootDir() {
        let omlPath = ''; //String(process.env['OML_ROOT']);
        // if not search in registery
        if (process.platform === 'win32') {
            try {
                const key = new winreg({
                    hive: winreg.HKLM,
                    key: '\\SOFTWARE\\Compose',
                });
                const items = await new Promise((resolve, error) => key.keys((err, result) => err === null ? resolve(result) : error(err)));
                let idx = items.findIndex(item => item.key.search('2022.3.6657') !== -1);
                if (idx !== -1) {
                    const key2 = new winreg({
                        hive: winreg.HKLM,
                        key: items[idx].key + '\\Capabilities',
                    });
                    const item = await new Promise((resolve, error) => key2.get('ApplicationIcon', (err, res) => err === null ? resolve(res) : error(err)));
                    omlPath = item.value;
                    idx = omlPath.lastIndexOf('hwx');
                    if (idx !== -1) {
                        omlPath = omlPath.slice(0, idx - 1);
                    }
                }
            }
            catch (error) {
                omlPath = '';
            }
        }
        return ''; //omlPath'';
    }
    async getFreePort() {
        return new Promise(resolve => {
            const ser = net.createServer();
            ser.listen(0, "127.0.0.1", () => {
                const addr = ser.address();
                ser.close();
                resolve(addr.port);
            });
        });
    }
    async delay(ms) {
        return new Promise((resolve) => setTimeout(resolve, ms));
    }
    spawnServer(args) {
        if (this.exeName === "Compose.exe") {
            process.env['PATH'] = path.join(this.omlRoot, '/hwx/bin/', this.platForm) + ";" +
                path.join(this.omlRoot, '/hw/bin/', this.platForm) + ";" + process.env['HW_FRAMEWORK'] +
                path.join('/bin/', this.platForm) + ";" + process.env['PATH'];
        }
        else {
            process.env['PATH'] = process.env['PATH'] + ";" +
                path.join(this.omlThirdParty, String(process.env['OML_INTEL_COMP'])) + ";" +
                path.join(this.omlThirdParty, String(process.env['OML_INTEL_MKL'])) + ";" +
                path.join(this.omlThirdParty, String(process.env['OML_FFTW'])) + ";" +
                path.join(this.omlThirdParty, String(process.env['OML_MATIO'])) + ";" +
                path.join(this.omlThirdParty, String(process.env['OML_HDF'])) + ";" +
                path.join(this.omlThirdParty, String(process.env['OML_QHULL']));
            console.log(process.env['PATH']);
        }
        let term = this.createTerminal(args);
        return term?.processId;
    }
    createTerminal(args) {
        if (vscode.window.terminals.length > 0) {
            if (vscode.window.activeTerminal?.name === 'oml') {
                return vscode.window.activeTerminal;
            }
        }
        const ws = vscode_1.workspace.workspaceFolders;
        const wsPath = ws ? ws[0].uri.fsPath : undefined;
        const terminalOptions = {
            name: 'oml',
            shellPath: this.omlPath,
            shellArgs: args,
            cwd: wsPath,
            env: {
                ['PATH']: process.env['PATH'],
                ['QT_QPA_PLATFORM_PLUGIN_PATH']: process.env['QT_QPA_PLATFORM_PLUGIN_PATH'],
                ['OML_APPDIR']: process.env['OML_APPDIR'],
                ['OML_HELP']: process.env['OML_HELP'],
                ['ALTAIR_HOME']: process.env['ALTAIR_HOME'],
                ['HW_ROOTDIR']: process.env['HW_ROOTDIR'],
                ['HW_UNITY_ROOTDIR']: process.env['HW_UNITY_ROOTDIR'],
                ['HW_FRAMEWORK']: process.env['HW_FRAMEWORK']
            }
        };
        try {
            omlTerminal = vscode_1.window.createTerminal(terminalOptions);
            omlTerminal.show(false);
        }
        catch (error) {
            vscode_1.window.showErrorMessage("Could not create OML terminal.");
        }
        return omlTerminal;
    }
    deleteTerminal(term) {
        if (term === omlTerminal) {
            omlTerminal.dispose();
            omlTerminal = undefined;
            this.isRunning = false;
            ;
        }
    }
    async runInTermianl(command) {
        if (omlTerminal) {
            omlTerminal.sendText(command);
            omlTerminal.show();
            await vscode.commands.executeCommand('workbench.action.terminal.scrollToBottom');
        }
    }
    async startConnection() {
        async function didOpenTextDocument(document) {
            if (document.languageId !== 'oml' && document.languageId !== 'OML') {
                return;
            }
        }
        return await this.createClient();
    }
    async restartConnection() {
        this.closeConnection();
        this.client = {};
        this.startConnection();
    }
    closeConnection() {
        this.isRunning = false;
        const promises = [];
        promises.push(this.client.stop());
        return Promise.all(promises).then();
    }
    async createClient() {
        this.outputChannel = vscode_1.window.createOutputChannel('OML');
        const port = await this.getFreePort();
        if (this.exeName === "Compose.exe") {
            const args = ['-withgui', '-toolbox', '-c', 'Compose', '-vscode-server', '-port', port.toString()];
            this.spawnServer(args);
        }
        else {
            const args = ['-vscode-server', '-port', port.toString()];
            this.spawnServer(args);
        }
        const serverOption = () => new Promise((resolve, reject) => {
            let socket = net.connect(port, 'localhost', () => {
                let result = {
                    writer: socket,
                    reader: socket
                };
                console.log("Connection Established");
                return resolve(result);
            });
            this.socket = socket;
            socket.addListener("data", (chunk) => this.recvData(chunk));
            socket.on('end', () => console.log('client disconnected from OML server'));
            socket.on('error', (e) => {
                console.log(e.message);
            });
        });
        const clientOptions = {
            // Register the server for OML documents
            documentSelector: [{ scheme: 'file', language: 'oml' }],
            outputChannel: this.outputChannel,
            synchronize: {
                // Notify the server about file changes to '.clientrc files contained in the workspace
                fileEvents: vscode_1.workspace.createFileSystemWatcher('**/.clientrc')
            }
        };
        // let the server warm up before client is created
        await this.delay(6000);
        let client = new node_1.LanguageClient('oml', 'OML Language Server', serverOption, clientOptions);
        extension_1.extensionContext.subscriptions.push(client);
        this.setClient(client);
        try {
            await client.start();
        }
        catch (e) {
            vscode.window.showErrorMessage('Could not start the OML language server. Pls make sure the enviornment is set');
        }
        return client;
    }
}
exports.LanguageConnection = LanguageConnection;
//# sourceMappingURL=languageConnection.js.map