"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.stopExecution = exports.runActiveFile = exports.newFileDocument = void 0;
const vscode = require("vscode");
const extension_1 = require("./extension");
async function newFileDocument() {
    vscode.workspace.openTextDocument({ language: 'oml' }).then((v) => vscode.window.showTextDocument(v));
}
exports.newFileDocument = newFileDocument;
async function runActiveFile() {
    if (!extension_1.langConn.isConnectionAlive()) {
        extension_1.langConn.restartConnection();
    }
    const actDoc = vscode.window.activeTextEditor?.document;
    if (!actDoc || actDoc.languageId !== 'oml') {
        return;
    }
    /*if (actDoc.isUntitled) {
        let text = actDoc.getText();
        let data: string = 'evalstring,' + text;
        langConn.sendRequest(data);
    }
    else {
        const isSaved = await actDoc.save();
        if (!isSaved) {
            vscode.window.showErrorMessage('Document could not be Saved. Pls check for write permission.');
        }
        let filePath: string = actDoc.uri.fsPath;
        let data: string = 'evalfile,' + filePath;
        langConn.sendRequest(data);
    }*/
    if (actDoc.isUntitled) {
        let text = actDoc.getText();
        let data = 'evalstring,' + text;
        extension_1.langConn.sendRequest(data);
        return;
    }
    await actDoc.save();
    let filePath = actDoc.uri.fsPath;
    let command = 'run(\'' + filePath + '\')';
    extension_1.langConn.runInTermianl(command);
}
exports.runActiveFile = runActiveFile;
async function stopExecution() {
    if (extension_1.langConn.isConnectionAlive()) {
        let data = 'interrupt,';
        extension_1.langConn.sendRequest(data);
    }
}
exports.stopExecution = stopExecution;
//# sourceMappingURL=languageCmds.js.map