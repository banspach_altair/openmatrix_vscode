"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deactivate = exports.activate = exports.langConn = exports.extensionContext = void 0;
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require("vscode");
const languageCmds = require("./languageCmds");
const languageConnection_1 = require("./languageConnection");
// This method is called when OML extension is activated
// The extension is activated the very first time the command is executed
async function activate(context) {
    // register commands from package.json
    context.subscriptions.push(vscode.commands.registerCommand('oml.Run', languageCmds.runActiveFile));
    context.subscriptions.push(vscode.commands.registerCommand('oml.Stop', languageCmds.stopExecution));
    context.subscriptions.push(vscode.commands.registerCommand('oml.newFileDocument', languageCmds.newFileDocument));
    // Create the language connection and start the client.
    exports.extensionContext = context;
    exports.langConn = new languageConnection_1.LanguageConnection();
    context.subscriptions.push(exports.langConn);
}
exports.activate = activate;
// This method is called when your extension is deactivated
async function deactivate() {
    await exports.langConn.dispose();
}
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map