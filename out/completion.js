"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SignatuareHelpProvider = exports.CompletionItemProvider = exports.CompletionData = void 0;
const vscode = require("vscode");
const extension_1 = require("./extension");
class CompletionData {
    constructor(first, last) {
        this.keywords_ = [];
        this.functions_ = [];
    }
    setKeywords(keywords) {
        this.keywords_ = keywords.split(' ');
    }
    setFunctions(functions) {
        this.functions_ = functions.split(' ');
    }
    getCompletionItems(target) {
        let regExp = RegExp('^' + target + '(?=).*');
        var completionList = this.keywords_.concat(this.functions_);
        let items = completionList.filter((str) => {
            return regExp.test(str);
        });
        items.sort((lhs, rhs) => {
            return lhs < rhs ? -1 : lhs > rhs ? 1 : 0;
        });
        return items;
    }
}
exports.CompletionData = CompletionData;
class CompletionItemProvider {
    constructor(compData) {
        this.completionData = compData;
    }
    provideCompletionItems(document, position, token, completionContext) {
        const items = [];
        if (token.isCancellationRequested) {
            return items;
        }
        const targetPosition = new vscode.Position(position.line, position.character - 1);
        const targetRange = document.getWordRangeAtPosition(targetPosition);
        const target = document.getText(targetRange);
        if (target.length > 1) {
            const matches = this.completionData.getCompletionItems(target);
            matches.forEach((match) => {
                items.push(new vscode.CompletionItem(match, vscode.CompletionItemKind.Function));
            });
        }
        return items;
    }
}
exports.CompletionItemProvider = CompletionItemProvider;
class SignatuareHelpProvider {
    constructor() {
        this.target_ = '';
        this.commas_ = 0;
    }
    async provideSignatureHelp(document, position, token, context) {
        this.commas_ = 0;
        this.target_ = '';
        if (token.isCancellationRequested || context === undefined) {
            return new vscode.SignatureHelp();
        }
        if (context.triggerCharacter === '(') {
            const targetPosition = new vscode.Position(position.line, position.character - 1);
            const targetRange = document.getWordRangeAtPosition(targetPosition);
            this.target_ = document.getText(targetRange);
        }
        else {
            // scan the line for target
            let lineText = document.lineAt(position.line).text;
            for (let i = position.character - 1; i > 0; --i) {
                if (lineText[i] === '(') {
                    const targetPos = new vscode.Position(position.line, i - 1);
                    const targetRange = document.getWordRangeAtPosition(targetPos);
                    this.target_ = document.getText(targetRange);
                    break;
                }
                else if (lineText[i] === ')') {
                    this.target_ = '';
                    break;
                }
                else if (lineText[i] === ',') {
                    ++this.commas_;
                }
            }
        }
        if (this.target_.length === 0) {
            return new vscode.SignatureHelp();
        }
        let data = 'signature,' + this.target_;
        await extension_1.langConn.sendRequest(data);
        return new Promise(resolve => {
            function provideSign(sign, commas) {
                let sigHelp = new vscode.SignatureHelp;
                sigHelp.activeSignature = 0;
                sigHelp.activeParameter = 0;
                let signs = sign.split(/(?<=\.)\n/);
                signs.forEach((data) => {
                    let sinfo = new vscode.SignatureInformation(data);
                    let params;
                    if ((params = data.match(/\((.*)\)/)) !== null) {
                        for (let i = 1; i < params.length; ++i) {
                            let param = params[i].split(',');
                            param.forEach(val => {
                                val.trim();
                                if (val.length) {
                                    sinfo.parameters.push(new vscode.ParameterInformation(val));
                                }
                            });
                            if (commas > 0 && commas === sinfo.parameters.length - 1) {
                                sigHelp.activeSignature = sigHelp.signatures.length;
                                sigHelp.activeParameter = commas;
                            }
                        }
                    }
                    sigHelp.signatures.push(sinfo);
                });
                resolve(sigHelp);
            }
            extension_1.langConn.addListener('signature', (sign) => {
                provideSign(sign, this.commas_);
            });
        });
    }
}
exports.SignatuareHelpProvider = SignatuareHelpProvider;
//export class OmlLanguageConfiguration implements vscode.LanguageConfiguration {}
/*export class SemanticTokensProvider implements vscode.DocumentSemanticTokensProvider {
    async provideDocumentSemanticTokens(document: vscode.TextDocument, token: vscode.CancellationToken): Promise<vscode.SemanticTokens> {
        const builder = new vscode.SemanticTokensBuilder();

    }
}*/ 
//# sourceMappingURL=completion.js.map